#include<iostream>
#include<cstdlib>

using namespace std;

class List
{
	private:
		typedef struct node
		{
			string title;
			string singer;
			node* next;
		}* nodePtr;
		nodePtr head;
		nodePtr tail;
		nodePtr temp;
		nodePtr curr;
	public:
		List();
		void EnQueue(string addTitle, string addSinger);
		void DeQueue();
		void PrintQueue();
};

List::List()
{
	head = NULL;
	tail = NULL;
	temp = NULL;
	curr = NULL;
}

void List::EnQueue(string addTitle, string addSinger)
{
	nodePtr x = new node;
	x->title = addTitle;
	x->singer = addSinger;
	x->next = NULL;
	if(head!=NULL)
	{
		curr=head;
		while (curr->next!=NULL)
		{
			curr=curr->next;
		}
		curr->next = x;
		tail = x;
	}
	else
	{
		head = x;
		tail = x;
	}
	
}

void List::DeQueue()
{
	if(head==NULL)
	{
		cout<<"Queue is empty!\n";
	}
	else if(head->next == NULL)
	{
		head = NULL;
		cout<<"Empty Queue!\n";
	}
	else
	{
		curr = head;
		head = head->next;
		curr->next = NULL;
		
	}
}

void List::PrintQueue()
{
	if (head==NULL)
	{
		cout<<"Nothing To Display....\n";
	}
	else
	{
		curr=head;
		while(curr!=NULL)
		{
			cout<<"Music Title: "<<curr->title<<" by: "<<curr->singer<<endl;
			curr=curr->next;
		}
	}
}

List list;

void switcher()
{
	cout<<"1. ADD OR EnQueue \n";
	cout<<"2. REMOVE OR DeQueue \n";
	cout<<"3. DISPLAY Queue\n";
	
	cout<<"Input choice: ";
	char z;
	cin>>z;
	cin.ignore(1,'\n');
	switch(z)
	{
		case '1':
		{
			string addTitle, addSinger;
			cout<<"Input Title: ";
			getline(cin,addTitle);
			cout<<"Input Singer: ";
			getline(cin,addSinger);
			list.EnQueue(addTitle,addSinger);
			cout<<"Music Already Saved!\n";
			break;
		}
		case '2':
		{
			list.DeQueue();
			break;
		}
		case '3':
		{
			list.PrintQueue();
			break;
		}
		default:
		{
			cout<<"Invalid Input!\n";
			break;
		}
	}
	switcher();
}

int main()
{
	cout<<"WELCOME TO MUSIC QUEUE\n\n";
	
	switcher();
}